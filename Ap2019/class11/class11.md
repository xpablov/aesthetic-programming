#### Class 11 | Week 17 | 23 Apr 2019: Advanced topics: Automation and Machine Learning
##### With Wed tutorial session and No Fri shutup and code session (set up your own study group)

### Messy notes:

#### Agenda:

1. What is (Critical) Machine Learning?
2. Peer-tutoring: node.js
3. Introducing ML js library
4. Discussion on Final Project
5. Walkthrough next mini-ex11:
---

[seebotchat](https://www.twitch.tv/videos/113582306)

<img src="http://andreasrefsgaard.dk/wp-content/uploads/2017/01/automatic-censorship.gif">
[An algorithm watching a movie trailer](https://andreasrefsgaard.dk/project/an-algorithm-watching-a-movie-trailer/) by Andreas Refsgaard

What happens when an object detection algorithm watches a movie trailer?

#### 1. What is (Critical) Machine Learning?
<img src="https://cdn.vox-cdn.com/thumbor/02if22ZptawJDp8QzaSVAEaeSMk=/0x0:1940x1244/920x613/filters:focal(815x467:1125x777):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/63372197/Screen_Shot_2019_04_08_at_1.36.11_PM.0.png">
[AI systems should be accountable, explainable, and unbiased, says EU](https://www.theverge.com/2019/4/8/18300149/eu-artificial-intelligence-ai-ethical-guidelines-recommendations?fbclid=IwAR1eDEdQUUoj5WueDkdxTMyJw9qF9C_RMNDNu1tkl0qvr2JwU_izoBAqrbw)

<img src="ml2.png">

<img src="ML3.png">

<img src="ml_translate.png">
ref: https://twitter.com/mit_csail/status/916032004466122758

<img src="ml_object.png">

[How China is building in all-seeing surveillance state by Washington Post](https://www.youtube.com/watch?v=uReVvICTrCM)

<img src="ml_assistant.png">

#### 3. Introducing ML js library
- [Teachable Machine](https://teachablemachine.withgoogle.com/)
- [ml5js](https://ml5js.org/)
  - all sources: https://github.com/ml5js/ml5-examples
  - image classification
  - Style transfer
  - word2vec
  - PoseNet with Webcam
- [Machine Learning Experiments](http://softwarestudies.projects.cavi.au.dk/index.php/Machine_Learning_Experiments)

**Discussion**

Try with all the examples above and based on this week's readings to discuss:

    - How could you relate your field and your life with Machine Learning?
    - How does the data effect the machine learning?
    - What is reading/seeing in machine learning? or How might machine learning changes the way how we read and see?
    - What is learning in machine learning?

#### 4. Discussion on Final Project
<img src="../class07/ap.png">

- [final project brief](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/all_mini_ex/finalProject.md)

- README: A single word document within 8-10 pages (max characters: 2400 per page include space) which has to be properly **written academically** (exclude references and notes).
  - With a proper "Introduction" and "Conclusion/Discussion"
  - What is your research agenda? Do you have a research question? What's the issue at stake here?
  - What is your software about (short description: what is it, how does it work and what do you want to explore)?
  - How does your work address at least one of the themes? Can you reflect about how does your work express the aesthetic of code/computational structure/computational culture? (You need to refer explicitly to any one of the assigned/suggested articles)
  - Open question: How do you see the project as a critical work in itself?

#### 5. Walkthrough next mini-ex11: Draft submission
- See [here](https://gitlab.com/siusoon/aesthetic-programming/tree/master/Ap2019/all_mini_ex) for miniEx11:
- check point1 : **28-Apr-2019** (SUN at 1500) Upload the draft final project + flowchart + reference list
- check point 2: Prepare a 5 mins oral feedback to the other groups.
