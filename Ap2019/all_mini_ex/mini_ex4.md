# Weekly mini ex4: due week 10, Monday night | CAPTURE ALL

**Objective:**
- To experiment various data capturing inputs, including audio, mouse, keyboard, web camera and beyond.
- To critically reflect upon the activity of data capture in digital culture (beyond buttons).

**Get some additional inspiration here:**
- [nonsense](http://siusoon.net/nonsense/) (2015) by Winnie Soon (look at the comment in the source code to get a sense of the work's concept)
- [Facebook Demetricator](https://bengrosser.com/projects/facebook-demetricator/) by Benjamin Grosser
- [LAUREN](http://lauren-mccarthy.com/LAUREN) by Lauren McCarthy
- [Feedback Machines](https://www.creativeapplications.net/processing/feedback-machines-mis-using-technology-non-linear-behavioural-systems/)

**Tasks (RUNME and README):**
1. Make sure you have read/watch the required readings/instructional videos and references (The focus on a button is just an example, you can apply the similar thinking on other kinds of tracking objects, such as face, mouse, audio, etc.)
2. Experiment various data capturing input and interactive devices, such as audio, mouse, keyboard, web camera, etc.
3. Develop a sketch that response loosely to the open call ["CAPTURE ALL"](https://transmediale.de/content/call-for-works-2015) from Transmediale 2015 that acts as the guideline for this mini exercise. (Imagine you are submitting a sketch/an artwork/a critical or speculative design work to Transmediale, hoping your sketch will be selected in the forthcoming exhibition. It is not a usual 'design brief' as you normally see with clear 'instructions' or 'problems to be solved' but it requires more conceptual thinking on problematizing the notion of 'capture all' and to think about what does it mean by 'data capture'.)
4. Upload your 'runme' to your own Gitlab account under a folder called **mini_ex4**. (Make sure your program can be run on a web browser)
4. Create a readme file (README.md) and upload to the same mini_ex4 directory (see [this](https://www.markdownguide.org/cheat-sheet) for editing the README). The readme file should contain the followings:
  - A screenshot/animated gif/video about your program (search for how to upload your chosen media and link to your gitlab)
  - A URL link to your program and run on a browser, see: https://www.staticaly.com/ or https://raw.githack.com/.
  - **Describe** about your sketch and explains what have been captured both conceptually and technically. How's the process of capturing?  
  - Together with the assigned reading and coding process, **how** might this ex helps you to think about or understand the data capturing process in digital culture?
5. Provide peer-feedback to 2 of your classmates on their works by creating "issues" on his/her gitlab corresponding repository. Write with the issue title "Feedback on mini_ex(?) by (YOUR FULL NAME)" (Feedback is due before next Wed tutorial class while the readme and runme are due on next Mon)

NB1: Feel Free to explore and experiment more syntax and computational structures.

NB2: The readme file should be within 5600 characters.

**mini exercise peer-feedback: guideline**
1. (Just think, no need to write) Think about what kind of feedback you think would be useful to others. What kind of feedback you want to receive by yourself?
2. Describe (basic) what is the program about, what syntaxes were used (any new syntax for you?), what does the work express?
3. Do you like the design of the program, and why? and which aspect do you like the most? How would you interpret the work?
4. How about the conceptual linkage on 'capture' beyond technical description and implementation? What's your thoughts/response on this?
