# Group work: Final Project Brief for Aesthetic Programming 2019

## Description:

The course introduces computer coding as an aesthetic, expressive, creative and critical endeavour beyond its functional application. It explores coding as a practice of reading, writing and building, as well as thinking with and in the world, and understanding the complex computational procedures that underwrite our experiences and realities in digital culture. Through coding practice, students are able to contextualize, conceptualize, articulate, design, build, write and run a piece of software.

By drawing upon different theoretical and conceptual texts from the course Aesthetic Programming and Software Studies, your group is required to conceptualize, design, implement and articulate a computational artifact that explore one, or more, of the weekly classes' theme, expressing the aesthetic of code/computational structure/computational culture. The idea should be built/reflect centrally upon any one of the assigned/suggested articles in Aesthetic Programming/Software Studies class but you are encouraged to have more than one references to support your idea. In this way, your work should demonstrate the ability to integrate practical programming skills and theoretical/conceptual understandings to articulate and develop computational artifacts, examining the aesthetic, cultural, social and political aspects of software.  

To begin with, there are few tips may help you to come up with a project idea:

1/ You **may** take a look again on the themes that we have used in our weekly classes for inspiration (Feel free to come up with your theme that is somehow related to below e.g love/life/labour/language).  

-	Coding Practice (Coding literacy/Why Program/Programming Practices/Aesthetic Programming)
-	Geometric shapes/ patterns/ emojis/ faces  
-	(Micro)Temporality/ Infinity/ Time/ Animation
-	Data Capture / Capture ALL / Interactivity
- Object Orientation / Object Oriented Programming
-	Automatisms/ Generativity / Rule-based systems
-	Language / Vocable Code / Expressivity / Electronic Literature
-	Queries / APIs
- Algorithms
- (Automation and Machine Learning)

2/ Take a look again on all the mini exercises and all the questions that were set. Is there any one that you want to explore further?

3/ Is there any assigned/suggested text (from both Aesthetic Programming and Software Studies) that you are especially connected with and you want to explore in a deeper way? (Do you particularly/partially agree or disagree on some of the points in the selected article? Why?)

4/ Is there any particular technical area that you want to explore and employ in this project?

5/ Take a look at previous students’ work: https://vimeo.com/groups/382805 (Just for reference, and please don't bound by the existing ideas. You have to login vimeo account to see that)

6/ Highly recommend you to read the suggested reading if you have an idea on the theme that you want to address.

## Deliverable:

1/ **RUNME:**  A piece of software written in p5.js (or a combination of HTML/CSS/JS/P5/node.js).
* Remember to include all external libraries and data such as images, font, text file, sound etc. Furthermore, if you have borrowed other sample code or ideas, please **cite** your sources in the code comments. The html file should be named as index.html

2/ **README:** A single word document within 8-10 pages (max characters: 2400 per page include space) which has to be **properly written academically** (exclude references and notes). The README document will be used as a base for oral exam.

The document should include a **title**, **a screen shot**, **a flow chart**, **a link to your video documentation/your browser work**, **references**, **links to related projects** (if there is any). Please export to a PDF and upload the file (see below #3 for the submission). Make sure everything is working especially your link (if not, please consider to do a video documentation, and write some guidelines about how should we view your work).

Regarding the README, it should address at least the following questions:
-	What is your software about (short description: what is it, how does it work and what do you want to explore)?
-	How does your work address at least one of the themes? Can you reflect about how does your work express the aesthetic of code/computational structure/computational culture? (You need to refer explicitly to any one of the assigned/suggested articles)
-	Open question: How do you see the project as a critical work in itself?

**NB: Be focused on your subject matter, and try to think about what's the problem or question that you want to address/raise in your work and back up your discussion and reflection with the chosen text(s) and the practice.**

3/ **Submission via Blackboard\Course Blog\**
- Make sure all your group's member names are on the readme file in the PDF format
- Zip your runme and readme into **one** file and upload to the blog (Make sure all your related libraries and data files are included and are in correct path. Please specify if you need us to run it on chrome/firefox.)
- For draft submission: Create the blog entry name: "Group X - Draft Final Project" by going to Blackboard\Course Blog\Create Blog Entry\
- For final submission: Create the blog entry name: "Group X- Final Project" by going to Blackboard\Course Blog\Create Blog Entry\

## Presentation:

Conduct a presentation within 10 mins with a software demo, articulating your work in both conceptual and technical levels. The presentation will follow by a short Q & A session (within 5 mins).

* Be selective of your presentation items; present those you think are key to the class
* Make sure you have tested your program in a classroom setting, such as projector, Internet connection, sound, slide, resolution etc.

## Timeline:

| Date         | Check point                                  
| ------------ |:-------------------------------------------  
| Week 12   , 19-Mar (TUE) | Final project brief introduction             
| Week 16   , 16-Apr (TUE) | Easter holiday, no class                                 
| Week 17   , 23 Apr (TUE) | Discussion on students' project
| Week 17   , 23-Apr (TUE) | Introduce mini_ex 11 > draft of final project - README                             
| Week 17   , 28-Apr (SUN) | **Upload the draft project + flowchart**                           
| Week 18   , 30-Apr (Tue) | Supervision and peer feedback
| Week 18   , 1-May (Wed) | No Wed Tutorial / Public Holiday
| Week 19   , 7-May  (TUE) | No class (Prepare for your final project)     
| Week 20   , 13-May  (Mon) | **FINAL GROUP PRESENTATION + SUBMISSION**     
| Week 24   , 12-14-Jun    | **ORAL EXAM**     

## Supervision Arrangement:
** Prepare a five mins oral feedback to the other group during the supervision: Location: 5361-144

| Time        | Group                             
| ------------ |:------------------------------------------  
| 30.Apr (Tue): 12.45-14.00 | Group 1, 2, 9            
| 30.Apr (Tue): 14.15-15.30 | Group 4, 5, 6                                 
| 30.Apr (Tue): 15.45-17.00 | Group 7, 8, 3                               

## Presentation Arrangement:
Date: 13 May 2019, Location: 5361-144

| Time        | Group                             
| ------------ |:------------------------------------------  
| 08.15-08.30 | Group 9            
| 08.35-08.50 | Group 8                                 
| 08.55-09.10 | Group 7                              
| BREAK                             
| 09.20-09.35 | Group 6                              
| 09.40-09.55 | Group 5     
| 10.00-10.15 | Group 4     
| BREAK
| 10.25-10.40 | Group 3                             
| 10.45-11.00 | Group 2     
| 11.05-11.20 | Group 1                             
| LUNCH BREAK
| 12.00-13.30 | ALL

## Oral Exam Arrangement:

**Requirement of entering exam A:**
- To pass the participation requirement and to enter Exam A, you need to
  - Read all the assigned reading and watch all the instructional video before coming to the class.
  - Do all the weekly mini exercises (mostly individual with a few group works)
  - Provide weekly peer feedback online
  - Peer-tutoring in a group format: within 20 mins in-class presentation with respondents
  - Active participation in class/instructor discussion and exercises
  - Submission of the final group project - in the form of a “readme” and a “runme” (software) packaged + in class presentation in the last lecture

Date: 12-14 JUN, schedule/Venue - TBC
- 30 mins preparation time
- 10 mins presentation (5 mins presentation on an on-site question + 5 mins presentation on the assigned question)
- 10 mins discussion together
- 5 mins discussion within examiners only
- 5 mins feedback on grade and exam

co-examinar: Magdalena Regina Tyzlik-Carver and Morten Breinbjerg

*Notes:
- feel free to bring your computer/pen/notes/blank paper if you think they help your exam
- no live coding
- Read again the course outline and learning outcomes on the course expectation
