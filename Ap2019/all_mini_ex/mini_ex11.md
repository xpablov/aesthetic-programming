# Weekly mini ex11: due on 💡Sunday 28 Apr | Draft Final Project (group work)

In order to prepare for the final project submission, this mini exercise is considered as a means towards this final goal.

**Tasks:**

1. This mini exercise is a group work, which is the same as your final project group.
2. Prepare a max 4 pages of synopsis that describes your final project idea - both technically and conceptually. What might be the problem of concern that you want to address in your final project?
3. You need to submit a draft flow chart and a reference list (on top of the written 4 pages).
4. Submit your draft in one single PDF format (Make sure all your group's member names are on the doc)
    - Create the blog entry name: "Group X - Draft Final Project" by going to Blackboard\Course Blog\Create Blog Entry\
5. As a group: Provide 5 mins each oral peer-feedback to the other two groups according to your [supervision schedule](https://gitlab.com/siusoon/aesthetic-programming/blob/master/Ap2019/all_mini_ex/finalProject.md) on 30 Apr.
  - What is the focus of the project?
  - What does the group want to address?
  - Is it specific enough to unfold the project/concept clearly?
  - Is the draft cleared to you (in terms of identifying/exploring the problems, addressing the specific theme of the course and how to use the readings)?
  - Can you get a sense of how their program would work through their flow chart and description?
  - Are there enough critical thoughts towards the reading(s)? What is the voice of the writers regarding the theoretical materials that they bring forward to the synopsis?
  - What are the new or original perspectives that this piece (both writing and program) brings to you?
  - Which part do you like the most and what might be the weakness of the synopsis?
  - Do you have any suggestion to improve for the group?
